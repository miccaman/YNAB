/*
 * ynab reserverd database
 */

PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;

Create Table ynabreserved (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  account nvarchar(28),
  amount FLOAT,
  expirationdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  creationdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  moddate      TIMESTAMP DEFAULT CURRENT_TIMESTAMP, 
  bankaccount nvarchar(35),
  active bit default 1
);

COMMIT;
