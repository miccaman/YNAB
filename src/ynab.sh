#!/usr/bin/env bash
set -euo pipefail
IFS=$';' # The IFS variable - which stands for Internal Field Separator
DB='data/ynabreserved.db'
databasereserved=0
data='true'

usage() {
  cat <<EOF
Usage: ./$(basename "$0") [-h] [-V]
You need a budget
Calculate your daily budget
Report to ynab-<bank>.csv

Options:

-h, --help      Print this help and exit
-V, --version   Print version
-a, --account   Amount in account
-r, --reserved  Reserved amount
-l, --lines     Spark lines in report
-b, --bank      Switch bank account (default Argenta) | KBC
EOF
  exit
}

#? -d, --database  Use reserved amount from database (always on)
version() {
  cat <<EOF
v0.8.1
EOF
  exit 0
}

usedata() {
  echo "Bankaccount is $1"
  databasereserved=$(sqlite3 $DB "SELECT ifnull(sum(amount),0) from ynabreserved where active = 1 AND expirationdate >= datetime() AND bankaccount = '$1';")
  REPORT='../report/ynab-'$1'.csv'
  checkfile $REPORT
}

parse_params() {
  bankaccount='Argenta'
  aamount=0
  sparkline=0
  ar=0
  aamount=0
  data='false'
  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -V | --version) version ;;
    -a | --aamount) # named parameter amount
      aamount="${2-}"
      shift
      ;;
    -r | --reserved) # named parameter reserved
      ar="${2-}"
      shift
      ;;
    -l | --lines) # named parameter lines
      sparkline="${2-}"
      shift
      ;;
    -b | --bank) # named parameter bank
      bankaccount="${2-}"
      shift
      ;;
     *) break ;;
    esac
    shift
  done
  return 0
}

parse_params "$@"

calculatespike() {
  echo "scale=2; 1.01*${MAX//[A-Z]/}" | bc
}

calculatemax() { # calculate max
  TMP=$(awk -F ';' '{print $4}' $REPORT | tail -n +2 | sort -n | tail -n 1)
  #? awk delimiter -F print COL 4 | skip header | sorteer -numeric | show 1 result
  echo $TMP
}

calculatemedian() { # calculate median
  TMP=$(sort -n $REPORT | awk ' { a[i++]=$4; } END { print a[int(i/2)]; }')
  echo $TMP
}

checkfile() { # CHECK if file exists
  if [ ! -e "$1" ] ; then
    touch "$1"
    echo "YYYYMMDD;  amount;reserved; dailybudget" > "$1"
  fi
}

case $bankaccount in
  KBC)
    usedata 'KBC' 
    ;;
  Argenta)
    usedata 'Argenta' 
    ;;
  *)
    echo "UNKNOWN BANK ACCOUNT"
    echo "see -h for more info"
    exit
    ;;
esac

ramount=$(echo "scale=2; $ar+$databasereserved" | bc -l)
echo "-------------------------------"
echo "- amount: ${aamount}"
echo "- reserv: ${ramount}"
if [ $data == 'true' ]; then
  echo "- reserved in data: ${databasereserved}"
fi
# LOG echo "- today : $(date +'%d')"
# LOG echo "- sparkl: ${sparkline}"
TD=$(date +'%d')
TDD=$(( 10#$TD-1)) # 10#$TD define as decimal
DTM=$(cal | grep -Ev [a-z] |wc -w)
# LOG echo "- #days : $DTM"
YNAB=$(echo "scale=2; ($aamount-($ar)-$databasereserved)/($DTM-$TDD)" | bc -l)
MEDIAN=$(calculatemedian)
MAX=$(calculatemax)
# echo "- median: ${MEDIAN}"
# echo "- max   :${MAX}"
SPIKE=$(calculatespike)
# echo "- spike : ${SPIKE}"
# # IF $YNAB is a spike (1.2 MAX) then show median as todays budget
if (( (${YNAB/\.*/}) > (${SPIKE/\.*/}) )); then
   echo "- $YNAB is a spike (1.01 MAX)"
   YNAB=$MEDIAN
fi
echo "-------------------------------"
echo " budget today = $YNAB EUR/d ($(date +'%d') $(date +'%b'))"
echo "-------------------------------"

# REPORT
printf "%4d%2s%2s; %7s; %7s; %5s EUR\n" $(date +'%Y') $(date +'%m') $(date +'%d') $aamount $ramount $YNAB >> $REPORT
LINES=$(wc -l < $REPORT)
SPARKL=$(($LINES - $sparkline))
awk -v l=$SPARKL 'NR>l {print $4}' $REPORT | spark

