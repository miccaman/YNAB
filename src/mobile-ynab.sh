#!/usr/bin/env bash
set -euo pipefail
IFS=$';' # The IFS variable - which stands for Internal Field Separator
# DB='data/ynabreserved.db'
# databasereserved=0
# data='true'

usage() {
  cat <<EOF
Usage: ./$(basename "$0") [-h] [-V]
You need a budget
Calculate your daily budget

Options:

-h, --help      Print this help and exit
-V, --version   Print version
-a, --account   Amount in account
-r, --reserved  Reserved amount
-b, --bank      Switch bank account (default Argenta) | KBC
EOF
# -l, --lines     Spark lines in report
  exit
}

#? -d, --database  Use reserved amount from database (always on)
version() {
  cat <<EOF
v0.8.0
EOF
  exit 0
}

usedata() {
  echo "Bankaccount is $1"
#  databasereserved=$(sqlite3 $DB "SELECT ifnull(sum(amount),0) from ynabreserved where active = 1 AND expirationdate >= datetime() AND bankaccount = '$1';")
#  REPORT='../report/ynab-'$1'.csv'
#  checkfile $REPORT
}

parse_params() {
  bankaccount='Argenta'
  aamount=0
#  sparkline=0
  ar=0
  aamount=0
#  data='false'
  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -V | --version) version ;;
    -a | --aamount) # named parameter amount
      aamount="${2-}"
      shift
      ;;
  #?  -d | --database) usedata ;; # option database
    -r | --reserved) # named parameter reserved
      ar="${2-}"
      shift
      ;;
    -l | --lines) # named parameter lines
      sparkline="${2-}"
      shift
      ;;
    -b | --bank) # named parameter bank
      bankaccount="${2-}"
      shift
      ;;
     *) break ;;
    esac
    shift
  done
  return 0
}

parse_params "$@"

# checkfile() { # CHECK if file exists
#   if [ ! -e "$1" ] ; then
#     touch "$1"
#     echo "YYYYMMDD;  amount;reserved; dailybudget" > "$1"
#   fi
# }

# echo "- bankaccount: ${bankaccount}"
case $bankaccount in
  KBC)
    usedata 'KBC' 
    ;;
  Argenta)
    usedata 'Argenta' 
    ;;
  *)
    echo "UNKNOWN BANK ACCOUNT"
    echo "see -h for more info"
    exit
    ;;
esac

# budget = (accamount-(sum future payments))/(DITM-(Today-1))
# ramount=$(echo "scale=2; $ar+$databasereserved" | bc -l)
echo "-------------------------------"
#? usedata 'Argenta'
echo "- amount: ${aamount}"
# echo "- total reserv: ${ramount}"
# echo "- reserved in data 0: ${databasereserved}"
# if [ $data == 'true' ]; then
#   echo "- reserved in data: ${databasereserved}"
# fi
echo "- today : $(date +'%d')"
# echo "- sparkl: ${sparkline}"
TD=$(date +'%d')
# echo $(( 10#$TD )) # define as decimal
TDD=$(( 10#$TD-1))
# echo $TDD
DTM=$(cal | grep -Ev [a-z] |wc -w)
echo "- days this month: $DTM"

# YNAB=$(echo "scale=2; ($aamount-($ar)-$databasereserved)/($DTM-$TDD)" | bc -l)
YNAB=$(echo "scale=2; ($aamount)/($DTM-$TDD)" | bc -l)
echo "-------------------------------"
echo " budget today = $YNAB EUR/d ($(date +'%d') $(date +'%b'))"
echo "-------------------------------"

# REPORT
# printf "%4d%2s%2s; %7s; %7s; %5s EUR\n" $(date +'%Y') $(date +'%m') $(date +'%d') $aamount $ramount $YNAB >> $REPORT
# LINES=$(wc -l < $REPORT)
#? echo $LINES
# SPARKL=$(($LINES - $sparkline))
# awk -v l=$SPARKL 'NR>l {print $4}' $REPORT | spark
#
