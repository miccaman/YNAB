
#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t' # The IFS variable - which stands for Internal Field Separator 

usage() {
  cat <<EOF
Usage: ./$(basename "$0") [-h] [-V] 
Bash kitchen sink

Options:

-h, --help      Print this help and exit
-V, --version   Print version
EOF
  exit
}

version() {
  cat <<EOF
v0.1.0
EOF
  exit 0
}

parse_params() {
  while :; do
    case "${1-}" in
    -h | --help) usage ;;
    -V | --version) version ;;
    *) break ;;
    esac
    shift
  done
  return 0
}

parse_params "$@"

# Define a cleanup function
cleanup() {
  echo "Cleaning up temporary files..."
  # rm -f ~/tmp/*.tmp
}

log() {
  echo "[ LOG ] $1"
  # Redirect to log file: echo "[INFO] $1" >> script.log
}

echo " ------------------------------"
echo "/       bash kitchen          /"
echo "------------------------------ "
# SLOG
#

# use datamash 
# Fix error using SED
# sed 's/ EUR//g' ynab-Argenta.csv
# datamash --field-separator=\; --header-in median 4 < ynab-Argenta.csv
# datamash: invalid numeric value in line 2 field 4: '  5.00 EUR'
# 
# datamash --field-separator=\; --header-in median 2 < ynab-Argenta.csv
# 
# datamash min 4 max 4 median 4 mean 4 < ynab.csv (need to convert to use , seperator
# BUDGET TODAY : /report/ynab.csv > calculate 
# [ ] median, 
# [x] average, : USE awk 
# top, 
# lowest
# # 
#
#
#

log "DATA"

# Trap the EXIT signal and call the cleanup function
trap cleanup EXIT
