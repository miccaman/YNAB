# YNAB

You need a budget - easy CLI calculator based on the enveloppe system

Basically this calculates the amount of money you can spend today, given that you want to divide the available amount equally over all days left in the current month. 

E.g. You have 2.1K on your bank account. And you know you will have to spend 1K on invoices (internet, rent, energy and so on). So you can spend 1.1K at once, leaving you bankrupt for the rest of the month. Or you divide the remaining 1.1K evenly over the remaining let's say 23 days. This will show you exactly how much is available in your enveloppe for today. 

```
Available today = (2.1K - 1K) / 23 d = 47.82
```

Adding sparklines (basic cli graph) will show you if your budget is getting tighter or not. 
Spending less than the available amount will increase your budget. Spending more will decrease your daily budget. 

I noticed quickly my spending behaviour quickly changed towards being more carefull at the beginning of the month. Being able to spend larger amounts at the end of month feels more rewarding and comfortable than dealing with the _unsufficient fund_.

This is a prelimanry step if you want FIRE. (Financial Independance Retire Early)

## Sample output

![Screenshot 01](asset/screenshot-01.png)


## Getting Started

Add a sqlite db to store reserved amounts, so you can set and _forget_ about reserved amounts

id | amount | currency | expirydate | creationdate | modificationdate | bankaccount | active


### Prerequisites

What things you need to install the software and how to install them

sparkl

```sh
sudo sh -c "curl https://raw.githubusercontent.com/holman/spark/master/spark -o /usr/local/bin/spark && chmod +x /usr/local/bin/spark"
```


bc

```
sudo dnf install bc
```


setup sqlite3 database

```
mkdir data
cat ynab-reserved.sql | sqlite3 data/ynabreserved.db
```

collect reserved amounts in sqlite

```
insert into ynabreserved (account,amount,expirationdate,bankaccount) values ('Internet',31.38,'2022-08-09 00:00:00','Banco');
```

## Mobile version

A [minified mobile version](src/mobile-ynab.sh) for use with Termux is available.

## Web version - WIP

A minimal webversion is under construction.

### Wireframe

![WEB version A0](asset/lofi-utext-A0.png)
![WEB version B1](asset/lofi-utext-B1.png)

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://codeberg.org/miccaman/YNAB/tags). 
## Authors

Tankred

## License

[GNU General Public License - Version 3](LICENSE)

## Acknowledgments

* Hat tip to anyone whose code was used
* [local storage sample](https://dev.to/alakkadshaw/javascript-localstorage-the-complete-guide-5a9a)
* [utext markdown wireframing](https://utext.github.io/#/flatly)
* [spark](https://raw.githubusercontent.com/holman/spark/master/spark)
