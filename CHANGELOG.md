# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [Released]

## [1.2.2] - 2024-08-22
### Changed
- Report using datamash: kitchensink 

## [1.2.1] - 2024-08-21
### Added
- Report using datamash 

## [1.2.0] - 2023-12-28
### Changed
- Display less msgs

## [1.1.3] - 2023-12-27
### Changed
- Display less msgs

## [1.1.2] - 2023-12-23
### Changed
- Display max enveloppe

## [1.1.1] - 2023-12-22
### Changed
- WIP Display max enveloppe

## [1.1.0] - 2023-12-21
### Changed
- Merge calculate max and median from DATA

## [1.0.6] - 2023-12-19
### Changed
- Calculate max from DATA

## [1.0.5] - 2023-12-15
### Changed
- Calculate max from DATA

## [1.0.4] - 2023-12-14
### Changed
- Calculate median from DATA

## [1.0.3] - 2023-12-13
### Changed
- Calculate median from DATA

## [1.0.2] - 2023-12-11
### Changed
- Calculate median from DATA

## [1.0.1] - 2023-11-30
### Changed
- Add link to SPARK

## [1.0.0] - 2023-11-05
### Changed
- PUBLISH MVP

## [0.9.4] - 2023-08-11
### Changed
- Add onclick event

## [0.9.3] - 2023-08-09
### Added
- Output hifi wireframe

## [0.9.2] - 2023-08-08
### Changed
- Start development hifi wireframe

## [0.9.1] - 2023-08-08
### Changed
- Explore localStorage 

## [0.9.0] - 2023-08-07
### Changed
- Bump version to 0.9.0

## [0.8.6] - 2023-08-07
### Changed
- Explore local storage sample

## [0.8.5] - 2023-08-04
### Changed
- Explore local storage sample
- Finetuning wireframe

## [0.8.4] - 2023-08-03
### Changed
- Explore local storage sample
### Added
- Wireframe web version start screen - WIP
- Wireframe web version start screen - UTEXT - WIP
- Utext-wireframe.md

## [0.8.3] - 2023-08-02
### Changed
- Explore local storage sample

## [0.8.2] - 2023-08-01
### Added
- Explore local storage sample

## [0.8.1] - 2023-07-31
### Changed
- Explore local storage sample

## [0.8.0] - 2023-06-05
### Added
- Add a wireframe

## [0.8.0] - 2023-06-05
### Changed
- Bump version 0.8.0

## [0.7.4] - 2023-06-04
### Added
- Slim mobile CLI version

## [0.7.3] - 2023-06-01
### Changed
- Fix egrep is obsolescent; using grep -E

## [0.7.2] - 2023-03-25
### Changed
- Implement bankaccount switch - KBC
- Add colomn bankaccount on sqlite db

## [0.7.0] - 2023-03-23
### Changed
- Implement bankaccount switch - KBC
- Fixed month date bug %m (and not %M !)

## [0.6.9] - 2023-03-22
### Changed
- Implement bankaccount switch - check if report file exists

## [0.6.8] - 2023-03-17
### Changed
- Implement bankaccount switch - check if report file exists

## [0.6.7] - 2023-03-16
### Changed
- Implement bankaccount switch - report file 

## [0.6.6] - 2023-03-15
### Changed
- Implement bankaccount switch - usedata 

## [0.6.5] - 2023-03-14
### Changed
- Implement bankaccount switch - usedata Fix

## [0.6.4] - 2023-03-13
### Changed
- Implement bankaccount switch - usedata Fix

## [0.6.3] - 2023-03-10
### Changed
- Implement bankaccount switch - usedata

## [0.6.2] - 2023-03-09
### Changed
- Implement bankaccount switch

## [0.6.1] - 2023-03-08
### Changed
- Implement bankaccount switch

## [0.6.0] - 2022-10-01
### Changed
- Fix spark error

## [0.5.3] - 2022-07-28
### Changed
- Update pricetag internet in readme

## [0.5.2] - 2022-06-20
### Changed
- Merge YNAB-2 readme img

## [0.5.1] - 2022-06-17
### Added
- readme img
### Changed
- README

## [0.5.0] - 2022-06-06
### Changed
- Merge reserveddb
- Fix typo in README

## [0.3.3] - 2022-05-31
### Changed
- Bugfix divide by zero (when database total is 0) 

## [0.3.2] - 2022-05-09
### Changed
- Finetuning database query: use datetime instead of date

## [0.3.1] - 2022-05-08
### Added
- Reserved database

## [0.3.0] - 2022-05-08
### Changed
- Readme

## [0.1.2] - 2022-05-08
### Added
- Report dir
- ynab.sh

## [0.1.1] - 2022-05-03
### Changed
- Add idea reserved.db

## [0.1.0] - 2022-05-03
### Added
- This CHANGELOG file

## [0.0.2] - 2022-05-03
### Changed
- Initial tag
